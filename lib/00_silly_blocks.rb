def reverser(&block)
  yield.reverse.split.reverse.join(" ")
end

def adder(value=1, &block)
  yield + value
end

def repeater(value=1)
  value.times {yield}
end
