def measure(number_of = 1)
    time_start = Time.now
    number_of.times {yield}
    (Time.now - time_start)/number_of
end
